#pragma once
#include <string>
#include <iostream>
#include "Employee.h"
#include "Interfaces.h"

using namespace std;

class Manager : public Employee, public Project
{
protected:
	int budget;
	string project;
	double contribution;
public:
	Manager() : budget(0), project(""), contribution(0) {};
	Manager(int id, string name, int budget, string project, double contribution) : Employee(id, name)
	{
		this->budget = budget;
		this->project = project;
		this->contribution = contribution;
	}
	void calculatePayment() override
	{
		payment = projectPayment(budget, contribution);
	}
	int projectPayment(int budget, double contribution) override
	{
		return (int)(budget * contribution);
	}
};

class ProjectManager : public Heading, public Manager
{
protected:
	int nSubordinates;
	int payForSubordinates;
public:
	ProjectManager() : nSubordinates(0), payForSubordinates(0) {};
	ProjectManager(int id, string name, int budget, string project, double contribution, int nSubordinates = 0, int payForSubordinates = 0) : 
		Manager(id, name, budget, project, contribution)
	{
		this->nSubordinates = nSubordinates;
		this->payForSubordinates = payForSubordinates;
	};
	void calculatePayment() override
	{
		payment = projectPayment(budget, contribution) + headingPayment(nSubordinates, payForSubordinates);
	}
	int headingPayment(int nSubordinates, int payForSubordinates) override
	{
		return nSubordinates * payForSubordinates;
	}
	void showInfo() override
	{
		cout << "ID: " << id << endl;
		cout << "Name: " << name << endl;
		cout << "Position: Project manager" << endl;
		cout << "Project: " << project << endl;
		cout << "Contribution to the project: " << contribution << endl;
		cout << "Number of subordinates: " << nSubordinates << endl;
		cout << "Pay for subordinate: " << payForSubordinates << " rubles" << endl;
		cout << "Payment: " << payment << " rubles" << endl << endl;
	}
};

class SeniorManager : public ProjectManager
{
public:
	SeniorManager() {};
	SeniorManager(int id, string name, int mainBudget, string project, double contribution, int nSubordinates = 0, int payForSubordinates = 0) : 
		ProjectManager(id, name, mainBudget, project, contribution, nSubordinates, payForSubordinates)
	{
		this->budget = mainBudget;
	};
	void calculatePayment() override
	{
		payment = projectPayment(budget, contribution);
	}
	void showInfo() override
	{
		cout << "ID: " << id << endl;
		cout << "Name: " << name << endl;
		cout << "Position: Senior manager" << endl;
		cout << "Projects: " << project << endl;
		cout << "Contribution to the projects: " << contribution << endl;
		cout << "Payment: " << payment << " rubles" << endl << endl;
	}
};
