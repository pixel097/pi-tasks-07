#include <string>
#include <iostream>
#include <fstream>
#include "Personal.h"
#include "Engineer.h"
#include "Manager.h"

int main()
{
	const int nStaff = 33;
	int n = 0;
	Employee * staff[nStaff];
	string id, name, worktime, rate, budget, project, contribution;
	string nSubordinates, payForSubordinates;
	ifstream f("staff.txt");
	while (getline(f, name))
	{
		if (name == "Cleaner")
		{
			getline(f, id);
			getline(f, name);
			getline(f, worktime);
			getline(f, rate);
			staff[n++] = new Cleaner(atoi(id.c_str()), name, atoi(worktime.c_str()), atoi(rate.c_str()));
			continue;
		}
		if (name == "Driver")
		{
			getline(f, id);
			getline(f, name);
			getline(f, worktime);
			getline(f, rate);
			staff[n++] = new Driver(atoi(id.c_str()), name, atoi(worktime.c_str()), atoi(rate.c_str()));
			continue;
		}
		if (name == "Programmer")
		{
			getline(f, id);
			getline(f, name);
			getline(f, worktime);
			getline(f, rate);
			getline(f, budget);
			getline(f, project);
			getline(f, contribution);
			staff[n++] = new Programmer(atoi(id.c_str()), name, atoi(worktime.c_str()), atoi(rate.c_str()),
				atoi(budget.c_str()), project, atof(contribution.c_str()));
			continue;
		}
		if (name == "Tester")
		{
			getline(f, id);
			getline(f, name);
			getline(f, worktime);
			getline(f, rate);
			getline(f, budget);
			getline(f, project);
			getline(f, contribution);
			staff[n++] = new Tester(atoi(id.c_str()), name, atoi(worktime.c_str()), atoi(rate.c_str()),
				atoi(budget.c_str()), project, atof(contribution.c_str()));
			continue;
		}
		if (name == "ProjectManager")
		{
			getline(f, id);
			getline(f, name);
			getline(f, budget);
			getline(f, project);
			getline(f, contribution);
			getline(f, nSubordinates);
			getline(f, payForSubordinates);
			staff[n++] = new ProjectManager(atoi(id.c_str()), name, atoi(budget.c_str()), 
				project, atof(contribution.c_str()), atoi(nSubordinates.c_str()), atoi(payForSubordinates.c_str()));
			continue;
		}
		if (name == "SeniorManager")
		{
			getline(f, id);
			getline(f, name);
			getline(f, budget);
			getline(f, project);
			getline(f, contribution);
			staff[n++] = new SeniorManager(atoi(id.c_str()), name, atoi(budget.c_str()), 
				project, atof(contribution.c_str()));
			continue;
		}
		if (name == "TeamLeader")
		{
			getline(f, id);
			getline(f, name);
			getline(f, worktime);
			getline(f, rate);
			getline(f, budget);
			getline(f, project);
			getline(f, contribution);
			getline(f, nSubordinates);
			getline(f, payForSubordinates);
			staff[n++] = new TeamLeader(atoi(id.c_str()), name, atoi(worktime.c_str()), atoi(rate.c_str()),
				atoi(budget.c_str()), project, atof(contribution.c_str()), atoi(nSubordinates.c_str()), atoi(payForSubordinates.c_str()));
			continue;
		}
    }
	for (n = 0; n < nStaff; n++)
	{
		staff[n]->calculatePayment();
		staff[n]->showInfo();
	}
	return 0;
}


