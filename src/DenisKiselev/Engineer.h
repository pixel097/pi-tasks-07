#pragma once
#include <string>
#include <iostream>
#include "Employee.h"
#include "Interfaces.h"

using namespace std;

class Engineer : public WorkTime, public Employee, public Project
{
protected:
	int rate;
	int budget;
	string project;
	double contribution;
public:
	Engineer() : rate(0), budget(0), project(""), contribution(0) {};
	Engineer(int id, string name, int worktime, int rate, int budget, string project, double contribution, int payment = 0) : 
		Employee(id, name, worktime, payment)
	{
		this->rate = rate;
		this->budget = budget;
		this->project = project;
		this->contribution = contribution;
	}
	void calculatePayment() override
	{
		payment = worktimePayment(worktime, rate) + projectPayment(budget, contribution);
	}
	int worktimePayment(int worktime, int rate) override
	{
		return worktime * rate;
	}
	int projectPayment(int budget, double contribution) override
	{
		return (int)(budget * contribution);
	}
};

class Tester : public Engineer
{
public:
	Tester() {};
	Tester(int id, string name, int worktime, int rate, int budget, string project, double contribution, int payment = 0) :	
		Engineer(id, name, worktime, rate, budget, project, contribution, payment) {};
	void showInfo() override
	{
		cout << "ID: " << id << endl;
		cout << "Name: " << name << endl;
		cout << "Position: Tester" << endl;
		cout << "Worked time: " << worktime << " hours" << endl;
		cout << "Rate: " << rate << " rubles" << endl;
		cout << "Project: " << project << endl;
		cout << "Contribution to the project: " << contribution << endl;
		cout << "Payment: " << payment << " rubles" << endl << endl;
	}
};

class Programmer : public Engineer
{
public:
	Programmer() {};
	Programmer(int id, string name, int worktime, int rate, int budget, string project, double contribution, int payment = 0) :	
		Engineer(id, name, worktime, rate, budget, project, contribution, payment) {};
	void showInfo() override
	{
		cout << "ID: " << id << endl;
		cout << "Name: " << name << endl;
		cout << "Position: Programmer" << endl;
		cout << "Worked time: " << worktime << " hours" << endl;
		cout << "Rate: " << rate << " rubles" << endl;
		cout << "Project: " << project << endl;
		cout << "Contribution to the project: " << contribution << endl;
		cout << "Payment: " << payment << " rubles" << endl << endl;
	}
};

class TeamLeader : public Programmer, public Heading
{
protected:
	int nSubordinates;
	int payForSubordinates;
public:
	TeamLeader() : nSubordinates(0), payForSubordinates(0) {};
	TeamLeader(int id, string name, int worktime, int rate, int budget, string project, double contribution, int nSubordinates, int payForSubordinates, int payment = 0) :
		Programmer(id, name, worktime, rate, budget, project, contribution, payment)
	{
		this->nSubordinates = nSubordinates;
		this->payForSubordinates = payForSubordinates;
	}
	void calculatePayment() override
	{
		payment = worktimePayment(worktime, rate) + projectPayment(budget, contribution) + headingPayment(nSubordinates, payForSubordinates);
	}
	int headingPayment(int nSubordinates, int payForSubordinates) override
	{
		return nSubordinates * payForSubordinates;
	}
	void showInfo() override
	{
		cout << "ID: " << id << endl;
		cout << "Name: " << name << endl;
		cout << "Position: Team leader" << endl;
		cout << "Worked time: " << worktime << " hours" << endl;
		cout << "Rate: " << rate << " rubles" << endl;
		cout << "Project: " << project << endl;
		cout << "Contribution to the project: " << contribution << endl;
		cout << "Number of subordinates: " << nSubordinates << endl;
		cout << "Pay for subordinate: " << payForSubordinates << " rubles" << endl;
		cout << "Payment: " << payment << " rubles" << endl << endl;
	}
};
