#pragma once
#include <string>
#include <iostream>

using namespace std;

class Employee
{
protected:
	int id;
	string name;
	int worktime;
	int payment;
public:
	Employee() : id(0), name(""), worktime(0), payment(0) {};
	Employee(int id, string name, int worktime = 0, int payment = 0)
	{
		this->id = id;
		this->name = name;
		this->worktime = worktime;
		this->payment = payment;
	}
	void setId(int id) { this->id = id; }
	void setName(string name) { this->name = name; }
	void setWorktime(int worktime) { this->worktime = worktime; }
	void setPayment(int payment) { this->payment = payment; }
	int getID() const{ return id; }
	string getName() const{ return name; }
	int getWorktime() const{ return worktime; }
	int getPayment() const{ return payment; }
	virtual void calculatePayment() = 0;
	virtual void showInfo() = 0;
};

