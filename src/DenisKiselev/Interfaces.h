#pragma once

class WorkTime
{
	virtual int worktimePayment(int worktime, int rate) = 0;
};

class Project
{
	virtual int projectPayment(int budget, double contribution) = 0;
};

class Heading
{
	virtual int headingPayment(int nSubordinates, int payForSubordinates) = 0;
};