#pragma once
#include <string>
#include <iostream>
#include "Employee.h"
#include "Interfaces.h"

using namespace std;

class Personal : public Employee, public WorkTime
{
protected:
	int rate;
public:
	Personal() : rate(0) {};
	Personal(int id, string name, int worktime, int rate, int payment = 0) : Employee(id, name, worktime, payment)
	{
		this->rate = rate;
	}
	void calculatePayment() override
	{
		payment = worktimePayment(worktime, rate);
	}
	int worktimePayment(int worktime, int rate) override
	{
		return worktime * rate;
	}
};

class Cleaner : public Personal
{
public:
	Cleaner() {};
	Cleaner(int id, string name, int worktime, int rate, int payment = 0) : Personal(id, name, worktime, rate, payment) {};
	void showInfo() override
	{
		cout << "ID: " << id << endl;
		cout << "Name: " << name << endl;
		cout << "Position: cleaner" << endl;
		cout << "Worked time: " << worktime << " hours" << endl;
		cout << "Rate: " << rate << " rubles" << endl;
		cout << "Payment: " << payment << " rubles" << endl << endl;
	}
};

class Driver : public Personal
{
public:
	Driver() {};
	Driver(int id, string name, int worktime, int rate, int payment = 0) : Personal(id, name, worktime, rate, payment) {};
	void showInfo() override
	{
		cout << "ID: " << id << endl;
		cout << "Name: " << name << endl;
		cout << "Position: driver" << endl;
		cout << "Worked time: " << worktime << " hours" << endl;
		cout << "Rate: " << rate << " rubles" << endl;
		cout << "Payment: " << payment << " rubles" << endl << endl;
	}
};

